var totalPhoneBill = require('../totalPhoneBill');
let assert = require("assert");

describe('this is a totalPhoneBill function' , function(){
    it("should return R7.45 if 2 calls are made and 3 sms's are sent" , function(){
        assert.equal('R7.45', totalPhoneBill('call, sms, call, sms, sms'));

    });
    it("should return R0.00 if no calls made or sms are sent" , function(){
        assert.equal('R0.00',totalPhoneBill(""));

    });
    it("should return R2.75 if 1 call is made" , function(){
        assert.equal("R2.75",totalPhoneBill("call"));

    });

});