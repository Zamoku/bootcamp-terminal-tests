module.exports = function mostProfitableDepartment(salesData){
  
    const salesMap = {}
    
    for(var i = 0; i < salesData.length;i++){
       const sale = salesData[i]
       
       salesMap[sale.department] = 0
     // console.log(salesMap)
    }
    for(var i = 0; i < salesData.length;i++){
       const sale = salesData[i]
       var currentDepartmentTotal = salesMap[sale.department]
       currentDepartmentTotal += sale.sales
       salesMap[sale.department] = currentDepartmentTotal
    }
   // console.log(salesMap)
    //console.log(currentDepartmentTotal)
    var currentMaxSales = 0
    var  currentDepart = "";
    for(const salesDepartment in salesMap){
     // console.log(salesDepartment)
     // console.log(salesMap[salesDepartment])
      const currentSaleDepartmentSales = salesMap[salesDepartment]
      
      if(currentSaleDepartmentSales > currentMaxSales){
        currentMaxSales = currentSaleDepartmentSales
        currentDepart = salesDepartment
       
        //console.log(currentSaleDepartmentSales)
      }
    }
     return currentDepart//console.log(currentDepart)
  }